package com.btsinfo.listviewlp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="comics.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseManager(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String strSqlE="create table Equipe(idE int primary key, nomE String, descE String, imgE String)";
        db.execSQL(strSqlE);
        String strSqlM="create table Hero(idE int,idH int, nomH String, vNom String, imgH String, pApp String, crea String, espece String, sexe String, taille String, poids String, statut String, pouvoir String, PRIMARY KEY (idE,idH), FOREIGN KEY (idE) REFERENCES Equipe(idE))";
        db.execSQL(strSqlM);

        //insertion des valeurs
        String e1 = "insert into Equipe values (1,'Avengers','Super héros Marvel', 'avengers/Avengers.jpg')";
        String e2 = "insert into Equipe values (2,'JLA','Super héros DC', 'jla/jla.png')";
        String e3 = "insert into Equipe values (3,'X-Men','Super héros mutants','xmen/xmen.png')";

        String h1 = "insert into Hero values (1,1,'Captain America','Steven Grant Rogers','avengers/capamerica.png','Captain America Comics #1,\nDécembre 1940','Joe Simon, Jack Kirby', 'Humain', 'Homme', '1m88', '100kg', 'En vie', '- Condition physique à l''apogée de l''évolution humaine')";
        String h2 = "insert into Hero values (1,2,'Guepe','Janet van Dyne' ,'avengers/guepe.png','Tales to Astonish #44,\nJuin 1963','Stan Lee, Ernest Hart, Jack Kirby', 'Humain', 'Femme', '1m62', '50kg', 'En vie', '- Changement de taille \n- Voler \n- Communication télépathique avec les insectes \n- Dards de guêpe ')";
        String h3 = "insert into Hero values (1,3,'Iron Man','Anthony Edward Stark' ,'avengers/ironman.png','Tales of Suspense #39,\nMars 1963' ,'Stan Lee, Larry Lieber, Don Heck, Jack Kirby','Humain','Homme','1m85','102kg','Mort', '- Son intelligence \n- Son armure')";
        String h4 = "insert into Hero values (1,4,'Sorcière Rouge','Wanda Maximoff' ,'avengers/sorciererouge.png','X-Men #4,\nMars 1964', 'Stan Lee, Jack Kirby','Humoin','Femme','1m70','60kg','En vie','- Modification de la réalité\n- Magie')";
        String h5 = "insert into Hero values (1,5,'Thor','Dieu du Tonnerre' ,'avengers/thor.png','Journey into Mystery #83,\nAoût 1962','Don Rico, Hy Rosen, Stan Lee, Jack Kirby, Larry Lieber','Dieu','Homme','1m98','290kg','En vie','- Sa force \n- Son marteau \n- Contrôler tous les éléments du temps \n- Et bien d''autres')";
        String h6 = "insert into Hero values (1,6,'Vif Argent',' Pietro Maximoff' ,'avengers/vifargent.png','X-Men #4,\nMars 1964','Stan Lee, Jack Kirby','Humain','Homme','1m82','79kg','Mort','- Vitesse surhumaine')";
        String h7 = "insert into Hero values (1,7,'Vision','Androïde','avengers/vision.png','Avengers #57,\nOctobre 1968','Roy Thomas, John Buscema','Androïde','Homme','1m90','136kg','Mort','- Manipuler sa propre densité')";
        String h8 = "insert into Hero values (2,8,'Aquaman', 'Arthur Curry','jla/aquaman.png','More Fun Comics #73,\nNovembre 1941','Paul Norris, Mort Weisinger','Hybride humain/atlante','Homme','1m83','147kg','Vivant','- Peut vivre sous l''eau\n- Vieillit plus lentement\n- Capacité physique surhumaine')";
        String h9 = "insert into Hero values (2,9,'Batman','Bruce Wayne' ,'jla/batman.png','Detective Comics #27,\nMai 1939','Bill Finger, Bob Kane','Humain','Homme','1m88','95kg','En vie','- Volonté Indomptable\n- Pic de la Performance Humaine\n- Furtif\n- Son armement\n- Et bien d''autres')";
        String h10 = "insert into Hero values (2,10,'Cyborg','Victor Stone' ,'jla/cyborg.png','DC Comics Presents #26,\nOctobre 1980','Marv Wolfman, George Pérez','Hybride humain/robot','Homme','1m98','175kg','En vie','- Force\n- Résistance surhumaine\n- Intelligence surhumaine\n- Sens ultra développés')";
        String h11 = "insert into Hero values (2,11,'Flash','Batholomew Henry Allen' ,'jla/flash.png','Showcase # 4,\nOctobre 1956','Robert Kanigher, Carmine Infantino','Méta humain','Homme','1m82','88kg','En vie','- Super-vitesse\n- Régénération accéléré\n- Voyage dans le temps\n- Endurance hors-norme')";
        String h12 = "insert into Hero values (2,12,'Green Lantern','Harold Jordan' ,'jla/greenlantern.png','Showcase #22,\nOctobre 1959)','John Broome, Gil Kane','Humain','Homme','1m88','84kg','En vie','- Anneau de la Green Lantern Corps')";
        String h13 = "insert into Hero values (2,13,'Superman','Clark Joseph Kent / Kal-El', 'jla/superman.png','Action Comics #1,\nAvril 1938','Jerry Siegel, Joe Shuster','Alien','Human','1m90','107kg','En vie','- Force surhumaine\n- Vitesse surhumaine\n- Endurance surhumaine\n- Sens ultra développés\n- Et bien d''autres')";
        String h14 = "insert into Hero values (2,14,'Wonder Woman','Diane de Themyscira', 'jla/wonderwoman.png','All Star Comics #8,\nDécembre 1941)','William Moulton Marston, Harry G. Peter','Amazone','Femme','1m80','75kg','En vie','- Force surhumaine\n- Vitesse surhumaine\n- Endurance surhumaine\n- Sens ultra développés\n- Et bien d''autres')";
        String h15 = "insert into Hero values (3,15,'Angel','Warren Worthington III' ,'xmen/angel.png','X-Men #1,\nSeptembre 1963','Stan Lee, Jack Kirby','Mutant','Homme','1m83','68kg','En vie','- Voler\n- Facteur guérisseur\n- Endurance surhumaine\n- Force surhumaine')";
        String h16 = "insert into Hero values (3,16,'Cyclope','Scott Summers', 'xmen/cyclope.png','X-Men #1,\nSeptembre 1963','Stan Lee, Jack Kirby','Mutant','Homme','1m90','88kg','En vie','- Émission de rafales de force optiques\n- Immunisé contre les pouvoirs d''Havok')";
        String h17 = "insert into Hero values (3,17,'Fauve','Henry Philip McCoy', 'xmen/fauve.png','X-Men #1,\nSeptembre 1963','Stan Lee, Jack Kirby','Mutant','Homme','1m80','182kg','En vie','- Force surhumaine\n- Endurance surhumaine\n- vitesse surhumaine\n- Agilité surhumaine\n- Réflexes surhumains\n- Facteur guérisseur')";
        String h18 = "insert into Hero values (3,18,'Iceberg','Bobby Drake' ,'xmen/iceberg.png','X-Men #1,\nSeptembre 1963','Stan Lee, Jack Kirby','Mutant','Homme','1m73','66kg','En vie','- Thermokinésie\n- Hydrokinésie\n- Cryokinésie\n- Vision thermique')";
        String h19 = "insert into Hero values (3,19,'Phénix','Jean Grey', 'xmen/phenix.png','X-Men #1,\nSeptembre 1963','Stan Lee, Jack Kirby','Mutant','Femme','1m68','59kg','En vie','- Télépathie\n- Télékinésie\n- Génération de feu cosmique\n- Altération de la réalité\n- Contrôle de l''espace et du temps\n- Contrôle de la force vitale\n- Manipulation du temps et de l''existence')";


        db.execSQL(e1);
        db.execSQL(e2);
        db.execSQL(e3);

        db.execSQL(h1);
        db.execSQL(h2);
        db.execSQL(h3);
        db.execSQL(h4);
        db.execSQL(h5);
        db.execSQL(h6);
        db.execSQL(h7);
        db.execSQL(h8);
        db.execSQL(h9);
        db.execSQL(h10);
        db.execSQL(h11);
        db.execSQL(h12);
        db.execSQL(h13);
        db.execSQL(h14);
        db.execSQL(h15);
        db.execSQL(h16);
        db.execSQL(h17);
        db.execSQL(h18);
        db.execSQL(h19);

        Log.i("DATABASE","onCreate invoqué");



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        String strSqlE="drop table Equipe";
        db.execSQL(strSqlE);
        String strSqlM="drop table Hero";
        db.execSQL(strSqlM);

        this.onCreate(db);

    }

    public ArrayList<Equipe> LectureEquipe()
    {
        ArrayList<Equipe> equipes= new ArrayList<>();
        String sqlE = "select * from Equipe";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlE,null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Equipe equipe = new Equipe(curseur.getInt(0),curseur.getString(1),curseur.getString(2),curseur.getString(3));
            equipes.add(equipe);
            curseur.moveToNext();
        }
        curseur.close();
        return  equipes;
    }

    public ArrayList<Hero> LectureHero(int idEquipe)
    {
        ArrayList<Hero> heros= new ArrayList<>();
        String sqlH = "select * from Hero where idE ="+idEquipe;
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlH,null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast())
        {
            Hero hero = new Hero(curseur.getInt(0),curseur.getInt(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getString(5),curseur.getString(6),curseur.getString(7),curseur.getString(8),curseur.getString(9),curseur.getString(10),curseur.getString(11),curseur.getString(12));
            heros.add(hero);
            curseur.moveToNext();
        }
        curseur.close();
        return  heros;
    }
}
