package com.btsinfo.listviewlp;

public class Hero {

    int idE;
    int idH;
    String NomH;
    String ImgH;
    String DescH;
    String VNom; //Vrai Nom
    String PApp; //1ère apparation
    String Crea; //Créateur(s)
    String Espece;
    String Sexe;
    String Taille;
    String Poids;
    String Statut;
    String Pouvoir;



    public Hero(int idE, int idH, String nomH, String descH, String imgH) {
        this.idE = idE;
        this.idH = idH;
        NomH = nomH;
        ImgH = imgH;
        DescH = descH;
    }

    public Hero(int idE, int idH, String nomH, String vNom,String imgH,String pApp,String crea, String espece, String sexe,String taille, String poids, String statut, String pouvoir)
    {
        this.idE = idE;
        this.idH = idH;
        NomH = nomH;
        VNom = vNom;
        ImgH = imgH;
        PApp = pApp;
        Crea = crea;
        Espece = espece;
        Sexe = sexe;
        Taille = taille;
        Poids = poids;
        Statut = statut;
        Pouvoir = pouvoir;
    }


    public int getIdE() {
        return idE;
    }

    public void setIdE(int idE) {
        this.idE = idE;
    }

    public int getIdH() {
        return idH;
    }

    public void setIdH(int idH) {
        this.idH = idH;
    }

    public String getNomH() {
        return NomH;
    }

    public void setNomH(String nomH) {
        NomH = nomH;
    }

    public String getImgH() {
        return ImgH;
    }

    public void setImgH(String imgH) {
        ImgH = imgH;
    }

    public String getDescH() {
        return DescH;
    }

    public void setDescH(String descH) {
        DescH = descH;
    }

    public String getVNom() {
        return VNom;
    }

    public void setVNom(String VNom) {
        this.VNom = VNom;
    }

    public String getPApp() {
        return PApp;
    }

    public void setPApp(String PApp) {
        this.PApp = PApp;
    }

    public String getCrea() {
        return Crea;
    }

    public void setCrea(String crea) {
        Crea = crea;
    }

    public String getEspece() {
        return Espece;
    }

    public void setEspece(String espece) {
        Espece = espece;
    }

    public String getSexe() {
        return Sexe;
    }

    public void setSexe(String sexe) {
        Sexe = sexe;
    }

    public String getTaille() {
        return Taille;
    }

    public void setTaille(String taille) {
        Taille = taille;
    }

    public String getPoids() {
        return Poids;
    }

    public void setPoids(String poids) {
        Poids = poids;
    }

    public String getStatut() {
        return Statut;
    }

    public void setStatut(String statut) {
        Statut = statut;
    }

    public String getPouvoir() {
        return Pouvoir;
    }

    public void setPouvoir(String pouvoir) {
        Pouvoir = pouvoir;
    }
}

