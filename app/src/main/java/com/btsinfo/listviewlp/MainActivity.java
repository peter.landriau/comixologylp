package com.btsinfo.listviewlp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    ArrayList<Equipe>listeEquipe;
    private DatabaseManager dbm;

    ListView lvListe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listeEquipe = new DatabaseManager(this).LectureEquipe();

        lvListe = (ListView)findViewById(R.id.ListView1);

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startViewActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        ListeAdapter listeAdapter = new ListeAdapter(this,listeEquipe);
        lvListe.setAdapter(listeAdapter);
    }
    private void startViewActivity(int i)
    {
        Equipe uneEquipe = listeEquipe.get(i);
        Intent intent = new Intent (this,MembreActivity.class);
        intent.putExtra("idE",uneEquipe.getIdE());

        startActivity(intent);
    }


}
