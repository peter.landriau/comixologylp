package com.btsinfo.listviewlp;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ListAdapterH extends ArrayAdapter<Hero> {

    Context context;
    public ListAdapterH(Context context, List<Hero> listeHero){

        super(context, -1, listeHero);
        this.context = context;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        Hero unHero;
        view = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.ligne, parent, false);

        } else {

            view = convertView;

        }

        unHero = getItem(position);
        TextView tvTitre1 = (TextView) view.findViewById(R.id.Titre1);
        TextView tvTitre2 = (TextView) view.findViewById(R.id.Titre2);
        ImageView imageView = (ImageView) view.findViewById(R.id.img1);

        tvTitre1.setText(unHero.getNomH());
        tvTitre2.setText(unHero.getVNom());

        AssetManager manager = context.getAssets();

        InputStream open = null;

        try {
            open = manager.open(unHero.getImgH());
            Bitmap bitmap = BitmapFactory.decodeStream(open);
            imageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return view;
    }
}
