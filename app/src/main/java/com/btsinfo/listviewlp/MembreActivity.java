package com.btsinfo.listviewlp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MembreActivity extends AppCompatActivity {

    TextView tvEquipe;

    ArrayList<Hero> listeHero;
    ArrayList<Equipe> listeEquipe;

    ListView lvListe;


    @Override
    protected void onCreate(Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_membre);

        tvEquipe = (TextView)findViewById(R.id.tvEquipe);
        lvListe = (ListView)findViewById(R.id.ListView2);

        Intent intent = getIntent();
        int idEquipe = intent.getIntExtra("idE",0);


        listeHero = new DatabaseManager(this).LectureHero(idEquipe);
        listeEquipe = new DatabaseManager(this).LectureEquipe();

        Equipe uneEquipe = listeEquipe.get(idEquipe-1);
        tvEquipe.setText(uneEquipe.getNomE());


        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    startViewActivity(i);
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
            ListAdapterH listAdapterH = new ListAdapterH(this,listeHero);
            lvListe.setAdapter(listAdapterH);
    }

    private void startViewActivity(int i)
    {
        Hero unHero = listeHero.get(i);
        Intent intent = new Intent (this,FicheActivity.class);
        intent.putExtra("idE",unHero.getIdE());
        intent.putExtra("idH",unHero.getIdH());

        startActivity(intent);
    }


}
