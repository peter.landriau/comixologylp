package com.btsinfo.listviewlp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class FicheActivity extends AppCompatActivity {

    TextView tVNomH,tVVNom,tVPApparition,tVCreateur,tVEspece,tVSexe,tVTaille,tVPoids,tVStatut,tVPower;
    ImageView imageView;
    ArrayList<Hero> listeHero;
    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fiche);



        imageView = (ImageView) findViewById(R.id.imgF);
        tVNomH = (TextView) findViewById(R.id.tVNomH);
        tVVNom = (TextView) findViewById(R.id.tVVNom);
        tVPApparition = (TextView) findViewById(R.id.tVPApparition);
        tVCreateur = (TextView) findViewById(R.id.tVCreateur);
        tVEspece = (TextView) findViewById(R.id.tVEspece);
        tVSexe = (TextView) findViewById(R.id.tVSexe);
        tVTaille = (TextView) findViewById(R.id.tVTaille);
        tVPoids = (TextView) findViewById(R.id.tVPoids);
        tVStatut = (TextView) findViewById(R.id.tVStatut);
        tVPower = (TextView) findViewById(R.id.tVPower);

        Intent intent = getIntent();
        int EquipeId = intent.getIntExtra("idE",0);
        int idH = intent.getIntExtra("idH",0);
        listeHero = new DatabaseManager(this).LectureHero(EquipeId);

        for(int i = 0; i < listeHero.size(); i++)
        {
            Hero unHero = listeHero.get(i);
            if(idH == unHero.getIdH())
            {
                tVNomH.setText(unHero.getNomH());
                tVVNom.setText(unHero.getVNom());
                tVPApparition.setText(unHero.getPApp());
                tVCreateur.setText(unHero.getCrea());
                tVEspece.setText(unHero.getEspece());
                tVSexe.setText(unHero.getSexe());
                tVTaille.setText(unHero.getTaille());
                tVPoids.setText(unHero.getPoids());
                tVStatut.setText(unHero.getStatut());
                tVPower.setText(unHero.getPouvoir());

                try {
                    InputStream open = getAssets().open(unHero.getImgH());
                    Bitmap bitmap = BitmapFactory.decodeStream(open);
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
